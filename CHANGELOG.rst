Version 0.2.2
=============

2018-11-13

- fix status for HTTP service when a error occurs before HTTP
- includes default configuration in package (fix #15)
- improve default configuration by addind a description to the HTTP watcher

Version 0.2.1
=============

2018-11-07

- fix in packaging : license field is not meant to receive the full License but only its name

Version 0.2.0
=============

2018-11-07

- web console is rewritten using Vue.js and Bootstrap 4
- Json API endpoints have been added
- fixes in documentation (it displayed Python instead of JSON as configuration examples)
- fix for TLS with Python 3.7
- packaging updates : use of setup.cfg for distributing and all configurations for development

Version 0.1.0
=============

2018-05-17

Initial release : Your invisible but loud monitoring pet

WatchGhost is a simple to install and simple to configure monitoring server. It is also a modern Python application making great use of asynchronous architecture.
