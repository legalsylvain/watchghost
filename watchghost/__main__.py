#!/usr/bin/env python3

# WatchGhost, your invisible but loud monitoring pet
# Copyright © 2015 Kozea

import asyncio
import logging
import os
import signal
import sys
from os.path import isfile

from tornado.options import options, parse_command_line, parse_config_file
from tornado.platform.asyncio import AsyncIOMainLoop
from watchghost import app, config, web  # noqa

DEFAULT_CONFIG = '/etc/watchghost.conf'


def reload_watchghost(signum, frame):  # noqa
    """ Restart WatchGhost on SIGHUP to reload configuration (and files) """
    log = logging.getLogger('watchghost')
    log.info('Signal {} received - reloading WatchGhost'.format(signum))
    try:
        os.execv(sys.executable, [sys.executable] + sys.argv)
    except OSError:
        log.error('Error during WatchGhost restart')


def main():
    AsyncIOMainLoop().install()

    watchghost_config = os.environ.get('WATCHGHOST_CONFIG')
    if watchghost_config:
        if not isfile(watchghost_config):
            logging.error('File {} does not exist', watchghost_config)
            sys.exit(1)
        parse_config_file(watchghost_config)
    elif isfile(DEFAULT_CONFIG):
        parse_config_file(DEFAULT_CONFIG)
    else:
        parse_command_line()

    log = logging.getLogger('watchghost')
    log.setLevel(logging.DEBUG if options.debug else logging.ERROR)

    signal.signal(signal.SIGHUP, reload_watchghost)

    config.read()

    log.debug('Listening to http://%s:%i' % (options.host, options.port))
    app.listen(options.port)
    loop = asyncio.get_event_loop()
    loop.run_forever()


if __name__ == '__main__':
    main()
