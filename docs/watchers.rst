Builtin Services used by Watchers
=================================

Default service
---------------

.. autoclass:: watchghost.services.Service

Ping
----
Default configuration:

.. code-block:: json

    {
        "repeat": 60,
        "timeout": 3,
        "ip_version": 4
    }

:timeout: time allowed before throwing a timeout
:ip_version: version of the IP protocol (4 or 6)

HTTP
----
Default configuration:

.. code-block:: python

    {
        "repeat": 60,
        "timeout": 5,
        "url": "",
        "ip_version": 4,
        "status": [
            ["error", [{"ok": false}, {"code": 400}, {"code": 401}, {"code": 402}, {"code": 403}, {"code": 404}, {"code": 405}, {"code": 406}, {"code": 407}, {"code": 408}, {"code": 409}, {"code": 410}, {"code": 411}, {"code": 412}, {"code": 413}, {"code": 414}, {"code": 415}, {"code": 416}, {"code": 417}, {"code": 418}, {"code": 419}, {"code": 420}, {"code": 421}, {"code": 422}, {"code": 423}, {"code": 424}, {"code": 425}, {"code": 426}, {"code": 427}, {"code": 428}, {"code": 429}, {"code": 430}, {"code": 431}]],
            ["warning", [{"code": 300}, {"code": 301}, {"code": 302}, {"code": 303}, {"code": 304}, {"code": 305}, {"code": 306}, {"code": 307}]],
            ["info", [{"code": 200}, {"code": 201}, {"code": 202}, {"code": 203}, {"code": 204}, {"code": 205}, {"code": 206}, {"code": 207}, {"code": 208}, {"code": 209}, {"code": 210}, {"code": 211}, {"code": 212}, {"code": 213}, {"code": 214}, {"code": 215}, {"code": 216}, {"code": 217}, {"code": 218}, {"code": 219}, {"code": 220}, {"code": 221}, {"code": 222}, {"code": 223}, {"code": 224}, {"code": 225}]],
            ["critical", [{"code": 500}, {"code": 501}, {"code": 502}, {"code": 503}, {"code": 504}, {"code": 505}, {"code": 506}, {"code": 507}, {"code": 508}, {"code": 509}, {"code": 510}, {"code": 511}]]
        ]
    }



:timeout: time allowed before throwing a timeout
:url: the URL checked by the watcher
:ip_version: version of the IP protocol (4 or 6)
:status: define the error to repport based on the http status in response

FTP
---

.. code-block:: json

    {
        "repeat": 60,
        "timeout": 5,
        "ip_version": 4,
        "url": ""
    }

:timeout: time allowed before throwing a timeout
:url: the URL checked by the watcher
:ip_version: version of the IP protocol (4 or 6)


SecuredSocket
-------------

Verify a certificate validity

.. code-block:: python

    {
        "repeat": 60,
        "ip_version": 4,
        "hostname": "",
        "port": 443,
        "minimum_days_left": 30,
        "status": [
            ["error", [
                {"hostname_verified": False},
                {"in_period": False},
                {"connected": False},
            ]],
            ["warning", [{"enough_days_left": False}]],
            ["info", [{}]]
        ]
    }


:ip_version: version of the IP protocol (4 or 6)
:hostname: name of the host of the certificate
:port: port emiting the the certificate
:minimum_days_left: number of days of validity left in the certificate before an alert
:status: error, warning or info can be throwed depending on different issues:
:hostname_verified: `true` if the hostname is effectively verified by the certificate.
:in_period: `true` the certificate is in the period of validity
:connected: `true` the certificate can be reached
:enough_days_left: `true` if there are more days of validity left than `minimum_days_left`


SSH
---

.. code-block:: json

    {
        "command": [],
        "status": [
            ("error", [{"exit_code": 2}]),
            ("warning", [{"exit_code": 1}]),
            ("info", [{"exit_code": 0}])
        ]
    }

:command: the command and the parameters to be executed on the remote server
:exit_code: code returned by the command


Shell
-----

.. code-block:: json

    {
        "timeout": 10,
        "command": "",
        "status": [
            ("error", [{"return_code": 2}]),
            ("warning", [{"return_code": 1}]),
            ("info", [{"return_code": 0}]),
            ("critical", [{}])
        ]
    }

:timeout: time allowed before throwing a timeout
:command: the command executed on the watchghost server
:return_code: the code returned by the command
